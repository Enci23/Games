#include <stdio.h>
#ifndef DEBUG
#define DEBUG(...) printf(__VA_ARGS__)
#endif

typedef struct{
    char n;
    int array[9];
}sudoku;

void ispis(sudoku S[9][9]){
    int i,j;
    for(i = 0; i < 9; i++){
        for(j = 0; j < 9; j++){
            printf("%c ",S[i][j].n);
            if((j+1)%3 == 0){
                printf(" ");
            }
        }
        if((i+1)%3 == 0){
                printf("\n");
            }
        printf("\n");
    }
}

void nesto(sudoku S[9][9], int m, int n){
    int i, j, k, p, tmp = S[m][n].n -'0' - 1;
    for(i = 0; i <= 6; i += 3){
        for(j = 0; j <= 6; j += 3){
            if((m >= i && m < i+3) && (n >= j && n < j+3)){
                for(k = i; k < i + 3; k++){
                    for(p = j; p < j + 3; p++){
                        S[k][p].array[tmp] = 1;
                    }
                }
            }
        }
    }
    for(i = 0; i < 9; i++){
        for(j = 0; j < 9; j++){
            if(i == m || j == n){
                S[i][j].array[tmp] = 1;
            }
        }
    }
    for(p = 0; p < 9; p++){
	    S[m][n].array[p] = 1;
	}
	//S[m][n].array[tmp] = 0;
}

void dva(sudoku S[9][9], int *prazno){
	int i, j, k, provjera, test;
	 for(i = 0; i < 9 ; i++){
            for(j = 0; j < 9; j++){
                test = 0;
                if(S[i][j].n == '*'){
                    for(k = 0; k < 9; k++){
                        test += S[i][j].array[k];

                        if(S[i][j].array[k] == 0){
                            provjera = k;
                        }
                    }
                    if(test == 8){
                            S[i][j].n = provjera+1 + '0';
                            nesto(S, i, j);
                            (*prazno)--;
                        }
                }
            }
        }
}


void jedan(sudoku S[9][9], int *prazno){
	int k, i, j, test, x, z, m, n;
	for(k = 0; k < 9; k++){          
    	for(i = 0; i <= 6 ; i += 3){
        	for(j = 0; j <= 6; j += 3){
	        		test = 0;
						for(m = i; m < i + 3; m++){
							for(n = j; n < j + 3; n++){
								test += S[m][n].array[k];
								if(S[m][n].array[k] == 0 && S[m][n].n == '*'){
									x = m;
									z = n; 
									}
								
							} if(test == 8){
								S[x][z].n = k + 1 + '0';
								nesto(S , x, z);
								(*prazno)--;
	            			}
						}
	        		}
	       		 }
	    	}
}


int main() {
    int i, j, k, prazno = 0;
    sudoku S[9][9];
    for(i = 0; i < 9; i++){
        for(j = 0; j < 9; j++){
            for(k = 0; k < 9; k++){
                S[i][j].array[k] = 0;
            }   
        }
    }
    for(i = 0; i < 9; i++){
        for(j = 0; j < 9; j++){
            scanf(" %c",&S[i][j].n);
            if(S[i][j].n == '*'){
                prazno++;
            }else{
                nesto(S, i, j);
            }
        }
    }
    
    for(i = 0; i < 50; i++){
    	jedan(S,&prazno);
    	dva(S,&prazno);
    }
    printf("%d\n",prazno);

    for(i = 0; i < 9; i++){
        for(j = 0; j < 9; j++){
            printf(" %c: ",S[i][j].n);

            for(int p = 0; p < 9; p++){
                printf("%d ",S[i][j].array[p]);
            }
            printf("\n");
        }
        printf("\n\n");
    }

   	ispis(S);
    return 0;
}