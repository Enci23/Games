#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef DEBUG
#define DEBUG(...) printf(__VA_ARGS__)
#endif

#define MAX 10 //max broj diskova
#define MAXX 15 //max sirina
#define MAXY 69 //max visina
#define RAZMAK 11 //minimalno potrebni razmak

typedef struct {
  int top; //pozicija top vrijednosti
  int index; //najlijevija pozicija diska na stupcu
  int *arr; //array, odnosno stack
} stack;

stack *stack_ctor(int size,int index) { //funkcija koja vraca napravljeni stack
  stack *s = malloc(sizeof(stack));
  s->top = -1;
  s->arr = malloc(size*sizeof(int));
  s->index = index;
  return s;
}

void stack_dtor(stack *s) { //funkcija koja brise stack
  free(s->arr);
  free(s);
}

int stack_is_empty(stack *s) { //funkcija koja provjerava ako je stack prazan
  return s->top==-1;
}


void stack_push(stack *s, int val,char polje[MAXX][MAXY]) { //funkcija koja dodaje disk, i crta ga u polje
  s->arr[++(s->top)] = val; //nadodaje vrijednost "val" na vrh stack-a
  int x = MAXX - 1 - s->top; //x-koo diska, odnosno gdje se po visini nalazi
  int k = s->index + RAZMAK + 1 + val; //polovica diska, k sluzi za crtanje najvise
  for(int j = s->index + RAZMAK - val; j < k; j++){ // j ide od skroz lijeve, do skroz desne strane diska
    if(j == s->index + RAZMAK - val || j == k - 1){
        polje[x][j] = '|'; //rubovi su prikazani s "|"
    }else{
        polje[x][j] = '='; //sredina je ispunjena s "="
    }
  }
}


void stack_pop(stack *s,char polje[MAXX][MAXY]) { //funkcija koja izbacuje s vrha stack-a
  if (stack_is_empty(s)) return;
  int x = MAXX - 1 - s->top;
  int k = s->index + RAZMAK + 1 + s->arr[s->top];
  for(int j = s->index + RAZMAK - s->arr[s->top]; j < k; j++){ //ista stvar kao u prijašnjoj funkcije, samo je brise
    if(j == s->index + RAZMAK){
        polje[x][j] = '|';
    }else{
        polje[x][j] = ' ';
    }
  }
  s->top--;
}

void unos_sipki(char polje[MAXX][MAXY]){ //unosi prazne sipke u polje
    for(int i = 0; i < MAXX; i++){
        for(int j = 0; j < MAXY; j++){
            if(j == 11 || j == 34 || j == 57){
                polje[i][j] = '|';
            }else{
                polje[i][j] = ' ';
            }
        }
    }
}

void ispis(char polje[MAXX][MAXY]){ //ispisuje polje
    for(int i = 0; i < MAXX; i++){
        for(int j = 0; j < MAXY; j++){
            printf("%c",polje[i][j]);
        }
        printf("\n");
    }
}

void zamjena(stack **out, stack **in, char polje[MAXX][MAXY]){//pop-a s jednog stacka (out), i pusha u drugi(in)
    stack_push((*in),(*out)->arr[(*out)->top],polje);
    stack_pop((*out),polje);
}

int provjera(stack **in, int n){ //provjerava ako je igrac prebacio sve diskove
    if((*in)->index == 0){
        return 0;
    }else if((*in)->top == n -1){
        return 1;
    }
    return 0;
}

int main() {
    int n; //broj diskova
    stack *prvi = stack_ctor(MAX, 0); //1. 2. 3. stup, najlijevije tocke su 0, 23, 46 
    stack *drugi = stack_ctor(MAX, 23);
    stack *treci = stack_ctor(MAX, 46);
    stack **tmp_in; //tmp za unos za korisnika
    stack **tmp_out;
    char polje[MAXX][MAXY];  //polje na kojem se crta sve 
    printf("Unesite broj krugova(1-10):\n");
    if(scanf("%d", &n) != 1){
        printf("Unesite broj!\n");
        return 0;
    }
    if(n < 1 ||n >10){
        printf("citaj upute!!\n");
        return 0;
    }
    unos_sipki(polje);

    for(int i = n; i > 0; i--){
        stack_push(prvi, i, polje);
    }
    
    ispis(polje);
    int in = 1, out = 1;
    printf("Morate premjestit sve diskove na drugu ili trecu sipku.\nZa pomicanje diskova koristite tipke 1, 2, 3, u obliku (2 3).\nZa izlazak iz igre unesite za in ili out 0.\n");
    while(1){
    printf("Unesite zamjenu:\n");
    if(scanf("%d %d", &out, &in) != 2){
        printf("bravo debilu, you happy now?\n");
        return 0;
    }
    if(in == 0 || out == 0){
        printf("izlazim\n");
        return 0;
    }
        if(in < 0 || in > 3 || out < 0 || out > 3){
            printf("pogresan unos!\n");
        }else{
            switch(in){ //ovisno o tome sto je korisnik unjeo, uzima adrese stackova
                case 1:
                tmp_in = &prvi;
                break;
                case 2:
                tmp_in = &drugi;
                break;
                case 3:
                tmp_in = &treci;
                break;
            }
            switch(out){
                case 1:
                tmp_out = &prvi;
                break;
                case 2:
                tmp_out = &drugi;
                break;
                case 3:
                tmp_out = &treci;
                break;
            }
            if((*tmp_out)->top == -1){ //provjerava da li je stack prazan
                printf("ne mozes uzet s praznog!\n");
            }else if((*tmp_out)->arr[(*tmp_out)->top] > (*tmp_in)->arr[(*tmp_in)->top]){  //provjerava ako korisnik pokusava stavit veci na manji disk
                printf("ne mozes veci disk na manji stavit!\n");
            }else{
                zamjena(tmp_out, tmp_in, polje);
                system("cls");
                ispis(polje);
                int c = provjera(tmp_in, n);
                if(c){
                    stack_dtor(prvi);
                    stack_dtor(drugi);
                    stack_dtor(treci);
                    printf("BRAVO, rijesili ste %d. razinu\n",n);
                    system("pause");
                    return 0;
                }

            }
        }
    }
    
    return 0;
}